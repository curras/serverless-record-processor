# 

This stack is designed to process records from a queue.

## Non Functional Requirements

* 6000 incoming messages per minute from a websocket.
* Each message analysis based on previous 200 messages.
* Don't need to keep more than 200 previous messages.
* Data delivered from 9am to 4pm.
* Only one websocket connection at a time.
* Written in Python.
* I don't want to pay when it's not doing anything.
* Budget: $$


## Architecture

* Data comes in from an external websocket connect (Fargate Container)
* SQS Queue 
* Lambda Function triggered by Queue
* DynamoDB Table for processed records
* Stream to Lambda
* S3 bucket for long term storage